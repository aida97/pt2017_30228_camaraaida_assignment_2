package proiect2TP;

import java.util.List;

public interface Generator<T> {

	public List<T> generate(int size, int arrivalMin, int arrivalMax, int processingMin, int processingMax);

}
