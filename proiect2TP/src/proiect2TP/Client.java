package proiect2TP;

public class Client {

	private int processingTime;
	private int arrivalTime;

	private String label;

	public Client(int pt, int at, String label) {
		processingTime = pt;
		arrivalTime = at;

		this.label = label;
	}

	public String toString() {
		return "Clientul " + label + " processing Time: " + Integer.toString(processingTime) + " Arrival Time: "
				+ Integer.toString(arrivalTime);
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public String getLabel() {
		return label;
	}

	public void setProcessingTime(int t) {
		processingTime = t;
	}

	public void setArrivalTime(int t) {
		arrivalTime = t;
	}

	public void setLabel(String l) {
		label = l;
	}
}
