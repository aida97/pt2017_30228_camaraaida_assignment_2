package proiect2TP;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller extends OutputStream implements Initializable {

	@FXML
	private TextField minArrival;

	@FXML
	private TextField maxArrival;

	@FXML
	private TextField minProcessing;

	@FXML
	private TextField maxProcessing;

	@FXML
	private TextField nrQueues;

	@FXML
	private TextField simulation;

	@FXML
	private TextField nrClienti;

	@FXML
	private TextField policy;

	@FXML
	private TextArea consoleTextArea;
	
	

	public Controller() {

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				appendText(String.valueOf((char) b));
			}
		};
		System.setOut(new PrintStream(out, true));
	}

	public void appendText(String str) {
		Platform.runLater(() -> consoleTextArea.appendText(str));
	}

	@FXML
	private void getInfo() {
		String arrivalMin = minArrival.getText();
		String arrivalMax = maxArrival.getText();
		String processingMin = minProcessing.getText();
		String processingMax = maxProcessing.getText();
		String queuesNr = nrQueues.getText();
		String simulationTime = simulation.getText();
		String clientiNr = nrClienti.getText();
		String poli = policy.getText();
		Service.returnInfo(clientiNr, arrivalMin, arrivalMax, processingMin, processingMax, queuesNr, simulationTime,
				poli);
		
	}

	
	@Override
	public void write(int b) throws IOException {
		// TODO Auto-generated method stub

	}

}
