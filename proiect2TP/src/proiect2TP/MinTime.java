package proiect2TP;

import java.util.List;

public class MinTime implements Strategy {

	@Override
	public int routeClient(Client c, List<Coada> listaCozi) {
		int min = listaCozi.get(0).getWaitingTime().get();
		for (Coada coada : listaCozi) {
			int time = coada.getWaitingTime().get();
			if (time < min) {
				min = time;
			}

		}
		for (int i = 0; i < listaCozi.size(); i++) {
			if (listaCozi.get(i).getWaitingTime().get() <= min) {
				listaCozi.get(i).getCoada().add(c);
				listaCozi.get(i).addWaitingTime(c.getProcessingTime());
				return i;
			}
		}
		return -1;
	}

}
