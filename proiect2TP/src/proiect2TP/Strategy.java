package proiect2TP;

import java.util.List;

public interface Strategy {

	public int routeClient(Client c, List<Coada> listaCozi);
}
