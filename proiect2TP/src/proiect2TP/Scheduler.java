package proiect2TP;

import java.util.List;

public class Scheduler {

	private Strategy strategy;

	public Scheduler(Strategy strategy) {
		this.strategy = strategy;
	}

	public int routeClient(Client c, List<Coada> cozi) {
		return strategy.routeClient(c, cozi);
	}

}
