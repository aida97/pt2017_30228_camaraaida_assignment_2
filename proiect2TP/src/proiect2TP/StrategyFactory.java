package proiect2TP;



public class StrategyFactory {

	//private static Strategy strategy;
	
	public static Strategy createStrategy(SelectionPolicy policy){
		if(policy==SelectionPolicy.SHORTEST_TIME){
			return new MinTime();
		}
		else{
			return new MinQueue();
		}
	}

	
}
