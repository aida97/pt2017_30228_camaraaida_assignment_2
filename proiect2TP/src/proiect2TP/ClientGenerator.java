package proiect2TP;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ClientGenerator implements Generator<Client> {

	@Override
	public List<Client> generate(int size, int arrivalMin, int arrivalMax, int processingMin, int processingMax) {
		List<Client> result = new ArrayList<Client>(size);
		Random rand = new Random();

		for (int i = 1; i <= size; i++) {
			int processingTime = rand.nextInt(processingMax);
			if (processingTime < processingMin) {
				int dif = processingMin - processingTime;
				processingTime += dif;
			}
			int arrivalTime = i;
			if (i != 1) {
				int r = rand.nextInt(arrivalMax);
				if (r < arrivalMin) {
					int dif = arrivalMin - r;
					r += dif;
				}

				arrivalTime = result.get(i - 2).getArrivalTime() + r;
				if (arrivalTime < arrivalMin) {
					int dif = arrivalMin - arrivalTime;
					arrivalTime += dif;
				}

			}

			Client client = new Client(processingTime, arrivalTime, Integer.toString(i));
			result.add(client);
		}

		return result;
	}

}
