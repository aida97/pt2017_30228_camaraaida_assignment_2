package proiect2TP;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class SimulationManager implements Runnable {

	private int nrClienti;
	private int minArrival;
	private int maxArrival;
	private int minProcessing;
	private int maxProcessing;
	private int nrQueues;
	private int simulation;
	private AtomicInteger currentTime;
	private Strategy strategy;
	private SelectionPolicy policy;
	private Scheduler scheduler;
	private List<Client> clienti;
	private List<Coada> cozi = new ArrayList<Coada>();

	

	public SimulationManager(int nrCli, int arrivalMin, int arrivalMax, int processingMin, int processingMax,
			int queuesNr, int simulationTime, SelectionPolicy poli) {
		nrClienti = nrCli;
		minArrival = arrivalMin;
		maxArrival = arrivalMax;
		minProcessing = processingMin;
		maxProcessing = processingMax;
		nrQueues = queuesNr;
		simulation = simulationTime;
		policy = poli;

		strategy = StrategyFactory.createStrategy(policy);
		scheduler = new Scheduler(strategy);

		ClientGenerator generator = new ClientGenerator();
		clienti = generator.generate(nrClienti, minArrival, maxArrival, minProcessing, maxProcessing);

		for (Client client : clienti) {
			System.out.println(client);
			
		}
		
		createQueues(nrQueues);
		QueueStart(clienti);
	}

	private void QueueStart(List<Client> clienti) {
		for (int i = 0; i < nrQueues; i++) {
			AtomicBoolean running = new AtomicBoolean(true);
			cozi.get(i).setQueueIsRunning(running);
			Thread coada = new Thread(cozi.get(i));
			coada.start();

		}

	}

	private void queueStop(List<Coada> cozi) {
		for (Coada coada : cozi) {
			coada.stop();
		}
	}

	private void createQueues(int nrQueues) {
		for (int i = 0; i < nrQueues; i++) {

			Coada coada = new Coada();
			cozi.add(coada);

		}

	}

	@Override
	public void run() {
		currentTime = new AtomicInteger(0);

		int maxClienti = -1;
		int avgServiceTime = 0;
		int avgWaitingTime = 0;
		int peakHour = 0;
		int nrCl = clienti.size();
		while (currentTime.get() < simulation) {

			System.out.println("timpul curent este: " + currentTime.get());

			for (Iterator<Client> iterator = clienti.iterator(); iterator.hasNext();) {
				Client client = iterator.next();
				if (client.getArrivalTime() == currentTime.get()) {
					int indexCoada = scheduler.routeClient(client, cozi);
					avgWaitingTime += cozi.get(indexCoada).getWaitingTime().get() - client.getProcessingTime();
					avgServiceTime += client.getProcessingTime();
					
					//System.out.println("am adaugat " + client + " la coada " + indexCoada);
					
					iterator.remove();

				}
			}

			for(Coada coada: cozi){
				Client[] vectorClienti=coada.getClienti();
				for(int i=0; i<coada.getCoada().size();i++){
					System.out.printf("cl %s     ", vectorClienti[i].getLabel());
				}
				System.out.println();
			}
			
			
			int totalClienti = 0;
			for (int i = 0; i < cozi.size(); i++) {
				totalClienti += cozi.get(i).getCoada().size();

			}
			if (maxClienti < totalClienti) {
				maxClienti = totalClienti;
				peakHour = currentTime.get();
			}

			for (Coada coada : cozi) {
				System.out.println("waiting time: " + coada.getWaitingTime() + "\n");
			}

			for (Coada coada : cozi) {
				if (coada.getWaitingTime().get() > 0) {
					coada.addWaitingTime(-1);

				}

			}

			sleep(1000);
			currentTime.incrementAndGet();

		}

		queueStop(cozi);
		sleep(1000);
		avgWaitingTime /= nrCl;
		avgServiceTime /= nrCl;
		System.out.println("Average waiting time: " + avgWaitingTime);
		System.out.println("Average service time: " + avgServiceTime);
		System.out.println("Peak hour: " + peakHour);
		System.out.println("nr max de clienti: " + maxClienti);

	}

	private void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			System.out.println("simulare intrerupta");
			e.printStackTrace();
		}
	}

	public String getCozi(){  //asta
		String rezultat=new String("a");
		for(Coada coada:cozi){
			for(Client client:coada.getCoada()){
				rezultat.concat("cl"+client.getLabel()+"    ");
			}
			rezultat.concat("\n");
		}
		return rezultat;
	}
}
