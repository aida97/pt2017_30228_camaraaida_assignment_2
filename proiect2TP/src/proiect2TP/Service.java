package proiect2TP;

public class Service {

	public static void returnInfo(String nrClienti, String arrivalMin, String arrivalMax, String processingMin,
			String processingMax, String queuesNr, String simulationTime, String poli) {
		int arrivMin = Parse.toInt(arrivalMin);
		int arrivMax = Parse.toInt(arrivalMax);
		int processMin = Parse.toInt(processingMin);
		int processMax = Parse.toInt(processingMax);
		int queues = Parse.toInt(queuesNr);
		int sim = Parse.toInt(simulationTime);
		int nrCli = Parse.toInt(nrClienti);
		if (arrivMin >= 0 && arrivMax >= 0 && processMin >= 0 && processMax >= 0 && queues >= 0 && sim >= 0
				&& nrCli >= 0) {
			SelectionPolicy policy = SelectionPolicy.SHORTEST_TIME;
			int ok = 1;
			if (poli.equals("min time")) {
				policy = SelectionPolicy.SHORTEST_TIME;
			} else {
				if (poli.equals("min queue")) {
					policy = SelectionPolicy.SHORTEST_QUEUE;
				} else {
					ok = 0;
				}
			}

			if (ok == 1) {

				SimulationManager simulare = new SimulationManager(nrCli, arrivMin, arrivMax, processMin, processMax,
						queues, sim, policy);
				Thread simulareThread = new Thread(simulare);
				simulareThread.start();
			} else {
				System.out.println("politica gresita!!");
			}

		}

	}
}
