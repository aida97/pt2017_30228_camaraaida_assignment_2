package proiect2TP;

import java.util.List;

public class MinQueue implements Strategy {

	@Override
	public int routeClient(Client c, List<Coada> listaCozi) {
		int min = listaCozi.get(0).getCoada().size();
		for (Coada coada : listaCozi) {
			int size = coada.getCoada().size();
			if (size < min) {
				min = size;
			}
		}
		for (int i = 0; i < listaCozi.size(); i++) {
			int size = listaCozi.get(i).getCoada().size();
			if (size == min) {
				listaCozi.get(i).getCoada().add(c);
				listaCozi.get(i).addWaitingTime(c.getProcessingTime());
				return i;
			}
		}
		return -1;
	}

}
