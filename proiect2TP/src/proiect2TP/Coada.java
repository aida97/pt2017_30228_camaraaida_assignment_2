package proiect2TP;

import java.util.concurrent.LinkedBlockingQueue;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada implements Runnable {

	private LinkedBlockingQueue<Client> coada = new LinkedBlockingQueue<Client>();
	private AtomicInteger waitingTime;
	private AtomicBoolean queueIsRunning;

	public Coada() {
		waitingTime = new AtomicInteger(0);
		queueIsRunning = new AtomicBoolean(false);
	}

	public void run() {

		while (queueIsRunning.get() == true) {
			Client currClient;
			currClient = coada.peek();
			if (currClient != null) {
				int ClientPT = currClient.getProcessingTime();
				sleep(ClientPT * 1000);
				
				coada.remove();

			}

		}
	}

	public void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			System.out.println("coada intrerupta");
			// e.printStackTrace();
		}
	}

	public void add(Client c) {
		coada.add(c);
		waitingTime.addAndGet(c.getProcessingTime());
	}

	public void stop() {
		queueIsRunning.set(false);
	}

	public Client[] getClienti() {
		Client[] clienti = new Client[coada.size()];
		coada.toArray(clienti);
		return clienti;
	}

	public void addWaitingTime(int val) {
		waitingTime.addAndGet(val);
	}

	public LinkedBlockingQueue<Client> getCoada() {
		return coada;
	}

	public AtomicInteger getWaitingTime() {
		return waitingTime;
	}

	public void setCoada(LinkedBlockingQueue<Client> coada) {
		this.coada = coada;
	}

	public void setWaitingTime(AtomicInteger waitingTime) {
		this.waitingTime = waitingTime;
	}

	public void setQueueIsRunning(AtomicBoolean queueIsRunning) {
		this.queueIsRunning = queueIsRunning;
	}

	public AtomicBoolean getQueueIsRunning() {
		return queueIsRunning;
	}
}
